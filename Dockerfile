FROM openjdk:11
WORKDIR /JavaApplication
COPY target /JavaApplication
COPY start.sh /JavaApplication
RUN chown -R www-data:www-data /JavaApplication
USER www-data
ENV arg1java="-jar"
ENV arg2java=" -Dspring.profiles.active=h2"
ENV arg3java="*.jar"
ENTRYPOINT ["/JavaApplication/start.sh"]
